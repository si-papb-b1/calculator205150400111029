package com.example.calculator205150400111029;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText fillone;
    private EditText filltwo;
    private EditText result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillone = (EditText) findViewById(R.id.fillone);
        filltwo = (EditText) findViewById(R.id.filltwo);
        result = (EditText) findViewById(R.id.result);

    }

    public void Add(View view) {
        result.setText(String.valueOf(Double.parseDouble(fillone.getText().toString()) + Double.parseDouble(filltwo.getText().toString())));
    }

    public void Subtract(View view){
        result.setText(String.valueOf(Double.parseDouble(fillone.getText().toString()) - Double.parseDouble(filltwo.getText().toString())));

    }

    public void Multiply(View view){
        result.setText(String.valueOf(Double.parseDouble(fillone.getText().toString()) * Double.parseDouble(filltwo.getText().toString())));

    }

    public void Divide(View view){
        result.setText(String.valueOf(Double.parseDouble(fillone.getText().toString()) / Double.parseDouble(filltwo.getText().toString())));

    }

    public void clearField(View view){
        result.getText().clear();
        fillone.getText().clear();
        filltwo.getText().clear();

    }
}